import configparser
import locale
import logging
import os
import random
import re
import shutil
import string
import subprocess
import time
from argparse import ArgumentParser
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
from functools import cached_property
from pathlib import Path
from queue import Queue
from tempfile import TemporaryDirectory
from textwrap import dedent
from threading import Thread
from typing import Union


class Config:
    _project_directory = os.path.abspath(os.path.dirname(__file__))
    _ini_file = Path(_project_directory).joinpath('config.ini')

    def __init__(self):
        self._config = configparser.ConfigParser()
        self._start_time = datetime.now().strftime('%y-%m-%d_%H-%M')
        self._validate_config()

    def _validate_config(self):
        if self._ini_file.exists():
            self._config.read(self._ini_file)
        else:
            raise FileNotFoundError(f'{self._ini_file} does not exist.')
        if not (self.dotcover_executable
                and self.mstest_executable
                and self.typemock_executable
                and self.test_threads
                and self.tpp_directory
                and self.test_settings
                and self.reports_directory
                and self.exclusions_file
                and self.open_reports):
            raise ValueError('Invalid configuration')

    @classmethod
    def create(cls):
        default_config = dedent(
            rf"""
            [Test]
            dotcover executable = dotcover.exe
            mstest executable = C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\Common7\IDE\MsTest.exe
            typemock executable = C:\Program Files (x86)\Typemock\Isolator\8.9\BuildScripts\TMockRunner.exe
            # Max number of test threads with which to perform concurrent testing
            # Note that a thread is also used to capture output of test threads
            test threads = 7
            
            [TPP]
            # Provide the path to the root of your TPP project
            directory = PLEASE SPECIFY
            
            [Reports]
            directory = {cls._project_directory}\reports
            exclusions file = {cls._project_directory}\exclusions.xml
            open when complete = yes
            """).lstrip()
        with open(cls._ini_file, 'w') as configfile:
            configfile.write(default_config)
        print(f'Created config file at {cls._ini_file}')

    @property
    def dotcover_executable(self) -> Union[Path, str]:
        try:
            path = Path(shutil.which(self._config['Test']['dotcover executable']))
            if path.exists() and path.is_file():
                return path
            raise Exception()
        except:
            raise ValueError('"mstest executable" is invalid or not specified')

    @property
    def mstest_executable(self) -> Union[Path, str]:
        try:
            path = Path(shutil.which(self._config['Test']['mstest executable']))
            if path.exists() and path.is_file():
                return path
            raise Exception()
        except:
            raise ValueError('"mstest executable" is invalid or not specified')

    @property
    def typemock_executable(self) -> Union[Path, str]:
        try:
            path = Path(shutil.which(self._config['Test']['typemock executable']))
            if path.exists() and path.is_file():
                return path
            raise Exception()
        except:
            raise ValueError('"typemock executabl"e is invalid or not specified')

    @property
    def test_threads(self) -> int:
        try:
            return int(self._config['Test']['test threads'])
        except:
            raise ValueError('"test threads" is invalid or not specified')

    @property
    def tpp_directory(self) -> Path:
        try:
            path = Path(self._config['TPP']['directory'])
            if path.exists() and path.is_dir():
                return path
            raise Exception()
        except:
            raise ValueError('"tpp directory" is invalid or not specified')

    @property
    def test_settings(self):
        path = Path(self.tpp_directory).joinpath(r'Venafi.Test\Unit\UnitTests.AnyCPU.testsettings')
        try:
            if path.exists() and path.is_file():
                return path
            raise Exception()
        except:
            raise ValueError(f'test settings file not found at {path}')

    @property
    def reports_directory(self) -> Path:
        try:
            path = Path(self._config['Reports']['directory'])
            if not path.exists() or not path.is_dir():
                path.mkdir(exist_ok=True)
            path = path.joinpath(self._start_time)
            if not path.exists() or not path.is_dir():
                path.mkdir(exist_ok=True)
            return path
        except:
            raise ValueError('"reports directory" is invalid or not specified')

    @property
    def exclusions_file(self) -> Path:
        try:
            path = Path(self._config['Reports']['exclusions file'])
            if path.exists() and path.is_file():
                return path
            raise Exception()
        except:
            raise ValueError('"exclusions" is invalid or not specified')

    @property
    def open_reports(self) -> bool:
        try:
            return str(self._config['Reports']['open when complete']).lower() == 'yes'
        except:
            return False

    @property
    def log_file(self) -> Path:
        return self.reports_directory.joinpath('cover_unit_tests.log')

    @cached_property
    def logger(self):
        logger = logging.getLogger('cover')
        logger.setLevel(logging.DEBUG)

        fh = logging.FileHandler(self.log_file)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
        logger.addHandler(fh)

        sh = logging.StreamHandler()
        sh.setLevel(logging.DEBUG)
        sh.setFormatter(logging.Formatter('%(message)s'))
        logger.addHandler(sh)

        return logger


class TestCoverage:
    def __init__(self, allow_unit: bool, allow_typemock: bool, exclude: bool):
        if not allow_unit and not allow_typemock:
            raise ValueError('Cannot disable Unit Tests and Typemock tests.')
        self._allow_unit = allow_unit
        self._allow_typemock = allow_typemock
        self._exclude = exclude

        self._config = Config()
        self._queue = Queue()
        self._completed_snapshots = []
        self._found_test_containers = []
        self._capture_output = False
        self._logger = self._config.logger

    @staticmethod
    def _new_snapshot_file(temp_dir: Path) -> Path:
        return Path(temp_dir).joinpath(f"{''.join((random.choice(string.ascii_letters) for _ in range(16)))}.dcvr")

    def _find_unit_test_containers(self):
        def is_unit_test(_filepath):
            _p = str(_filepath).lower()
            return "unittest" in _p or "unit.test" in _p or "test.unit" in _p

        # Find valid test containers
        search_pattern = Path(self._config.tpp_directory).joinpath('Bin')
        self._found_test_containers = [d for d in search_pattern.glob('*.dll') if is_unit_test(d)]

    def _output_thread(self):
        # Capture entire test outputs as they complete using output thread
        self._capture_output = True
        while self._capture_output:
            test_container, is_typemock, snapshot, output = self._queue.get()
            self._logger.info(
                f'Covered {"typemock" if is_typemock else ""} tests in {test_container}. Saved in {snapshot}.')
            self._logger.info(str(output))
            total_containers = len(self._found_test_containers) * (
                2 if self._allow_unit and self._allow_typemock else 1)
            self._logger.info(
                f'Completed snapshot {len(self._completed_snapshots)}/{total_containers}.')
            self._queue.task_done()

    def _cover_test_container(self, test_container: str, snapshot: Path, is_typemock: bool):
        try:
            arguments = ' '.join((
                f'/testcontainer:{test_container}',
                f'/testsettings:{self._config.test_settings}',
                f'/category:{"" if is_typemock else "!"}Typemock'
            ))
            command = [
                self._config.dotcover_executable, 'cover',
                f'--TargetExecutable:{self._config.mstest_executable}',
                f'--TargetArguments:{arguments}',
                f'--Output:{snapshot}.',
                '--AttributeFilters=System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute;'
            ]
            if is_typemock:
                command.insert(0, self._config.typemock_executable)
            p = subprocess.run(args=command, capture_output=True)
            self._queue.put((test_container, is_typemock, snapshot, p.stdout.decode(locale.getpreferredencoding())))
            self._completed_snapshots.append(snapshot)
        except Exception as e:
            self._logger.critical(e)

    def _generate_report(self, snapshot: Path, report: Path, use_exclusions: bool):
        command = [
            'dotcover', 'report',
            f'/Source:{snapshot}',
            f'/Output:{report}',
            '/ReportType:HTML'
        ]
        if use_exclusions:
            command.insert(2, self._config.exclusions_file)
        subprocess.run(args=command)

    def find_test_failures(self):
        with open(self._config.log_file, 'r') as log:
            numbers = re.findall(pattern=r'Failed\s\s(\d+)\n', string=log.read())
        failures = sum([int(n) for n in numbers])
        return failures

    def run(self):
        start_time = time.perf_counter()

        # Find valid test dlls
        self._logger.info(f'Searching for unit test dlls')
        self._find_unit_test_containers()
        self._logger.info(f'Found {len(self._found_test_containers)} test containers.')
        self._logger.info(f'Time to find unit test dlls: {time.perf_counter() - start_time}s')
        self._logger.info('#' * 100)

        # Create a temporary directory for snapshots
        with TemporaryDirectory() as temp_dir:
            cover_start_time = time.perf_counter()

            # Start output thread to capture concurrent outputs from threadpool
            t = Thread(target=self._output_thread, daemon=True)
            t.start()

            # Cover unit tests using a thread pool
            if self._allow_unit and self._allow_typemock:
                self._logger.info(f'Covering unit and Typemock tests.')
                report_base = 'Unit_and_Typemock'
            elif self._allow_unit:
                self._logger.info(f'Covering unit tests.')
                report_base = 'Unit'
            elif self._allow_typemock:
                self._logger.info(f'Covering Typemock tests.')
                report_base = 'Typemock'
            else:
                raise Exception('Should not reach here')
            with ThreadPoolExecutor(max_workers=self._config.test_threads) as executor:
                for c in self._found_test_containers:
                    if self._allow_unit:
                        snapshot = self._new_snapshot_file(temp_dir=temp_dir)
                        executor.submit(self._cover_test_container, test_container=c, snapshot=snapshot,
                                        is_typemock=False)
                    if self._allow_typemock:
                        snapshot = self._new_snapshot_file(temp_dir=temp_dir)
                        executor.submit(self._cover_test_container, test_container=c, snapshot=snapshot,
                                        is_typemock=True)

            if self._config.log_file.exists():
                failures = self.find_test_failures()
                if failures:
                    self._logger.warning(f'{failures} tests failed.')

            self._logger.info(f'Time to cover all tests: {time.perf_counter() - cover_start_time}s')
            self._logger.info('#' * 100)

            # Stop capturing output
            self._capture_output = False
            t.join(timeout=10)

            merge_start_time = time.perf_counter()

            # Merge snapshots into one
            self._logger.info(f'Merging {len(self._completed_snapshots)} snapshots.')
            merged_snapshot = self._new_snapshot_file(temp_dir=temp_dir)
            command = [
                'dotcover', 'merge',
                f'--Source:{";".join([str(s) for s in self._completed_snapshots if s])}',
                f'--Output:{merged_snapshot}'
            ]
            subprocess.run(args=command)

            self._logger.info(f'Time to merge snapshots: {time.perf_counter() - merge_start_time}s')
            self._logger.info('#' * 100)

            report_start_time = time.perf_counter()

            if self._exclude:
                self._logger.info('Generating report with exclusions.')
                report_base = f'{report_base}_excluded'
            else:
                self._logger.info('Generating report.')
            report = Path(self._config.reports_directory).joinpath(rf'{report_base}\report.html')

            # Generate reports
            self._generate_report(
                snapshot=merged_snapshot,
                report=report,
                use_exclusions=self._exclude
            )
            self._logger.info(f'Generated coverage report {report}.')

            self._logger.info(f'Time to generate reports: {time.perf_counter() - report_start_time}s')
            self._logger.info('#' * 100)

        # Open reports
        if self._config.open_reports:
            self._logger.info(f'Opening report.')
            os.system(rf"start {report}")

        self._logger.info(f'Total time complete: {time.perf_counter() - start_time}s')


def main():
    parser = ArgumentParser(description='A Python script for covering TPP unit tests using JetBrains dotCover.')

    subparsers = parser.add_subparsers(dest='command')
    subparsers.add_parser('config', help='Generate a config file.')
    cover = subparsers.add_parser('cover', help='Cover TPP unit tests')

    exclusive = cover.add_mutually_exclusive_group()
    cover.add_argument('-e', '--exclude', action='store_true', help='Apply exclusions from exclusions file.')
    exclusive.add_argument('-t', '--typemock-only', action='store_true', help='Only cover Typemock unit tests.')
    exclusive.add_argument('-u', '--unit-only', action='store_true', help='Only cover non-Typemock unit tests.')

    args = parser.parse_args()
    if args.command == 'config':
        Config.create()
    elif args.command == 'cover':
        if args.typemock_only:
            TestCoverage(allow_unit=False, allow_typemock=True, exclude=args.exclude).run()
        elif args.unit_only:
            TestCoverage(allow_unit=True, allow_typemock=False, exclude=args.exclude).run()
        else:
            TestCoverage(allow_unit=True, allow_typemock=True, exclude=args.exclude).run()


if __name__ == '__main__':
    main()
