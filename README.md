# dotCover Local Runner

```yaml
Author: Taylor Dall
```

This tool utilizes the dotCover Command Line Tools to perform .NET code coverage on TPP unit tests. It supports running
unit tests with and without [Typemock](https://www.typemock.com/isolator-product-page/).

## Prerequisites

Clone this repo in a TPP Developer Environment, and make sure you have installed the following:

- Python 3.6+
- [dotCover Command Line Tools](https://www.jetbrains.com/dotcover/download/#section=commandline)
- MSTest (Microsoft Visual Studio 2019)
- Typemock 8.9+

## Usage

### Configuration

To use `cover_tpp.py` to execute unit tests with code coverage, you must first create a `config.ini` file in the same
directory.

You can generate `config.ini` with the following command:

```commandline
python cover_tpp.py config
```

The generated `config.ini` will look like this:

```ini
[Test]
dotcover executable = dotcover.exe
mstest executable = C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\Common7\IDE\MsTest.exe
typemock executable = C:\Program Files (x86)\Typemock\Isolator\8.9\BuildScripts\TMockRunner.exe
# Max number of test threads with which to perform concurrent testing
# Note that a thread is also used to capture output of test threads
test threads = 7

[TPP]
# Provide the path to the root of your TPP project
directory = PLEASE SPECIFY

[Reports]
directory = <path_to_this_repo>\reports
exclusions file = <path_to_this_repo>\exclusions.xml
open when complete = yes
```

`<path_to_this_repo>` will be automatically detected; however, you must specify the directory to the root of your TPP
project. Make any other necessary changes to `config.ini`  before executing code coverage.

### Executing Code Coverage

Run the following command to perform code coverage analysis on both typemock and non-typemock unit tests in your TPP
project:

```commandline
python cover_tpp.py cover
```

#### Typemock Unit Tests

To cover only typemock unit tests, use the flag `--typemock-only`/`-t`:

```commandline
python cover_tpp.py cover --typemock-only
```

#### Non-Typemock Unit Tests

To cover only non-typemock unit tests, use the flag `--unit-only`/`-u`:

```commandline
python cover_tpp.py cover --unit-only
```

### Coverage Reports

Each successful run of `cover_tpp.py` will generate a timestamped report in the `reports` directory.

A report is viewable by opening its `report.html` in your browser. If `open when complete = yes` in your `config.ini`,
the report will open automatically in your browser when the coverage completes.

#### Exclusions

You can define areas of code to exclude in the coverage report in `exclusions.xml`. This is passed to dotCover when
generating the coverage report when you run
`cover_tpp.py` with the flag `--exclude`/`-e`.

```commandline
python cover_tpp.py cover --exclude
```

#### Logs

The output from executing test coverage is logged in `cover_unit_tests.log`
within the run's report folder.

